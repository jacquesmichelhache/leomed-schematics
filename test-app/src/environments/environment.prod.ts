const subdomain = window.location.hostname.split('.')[0];
const stagingSubdomains = ['staging', 'staging2', 'demo'];
const isStagingEnv = stagingSubdomains.includes(subdomain);

// prod
const hubBaseUrl = 'https://login.leomed.co';
const hubClientId = '537d3ed6-b8dd-403c-9471-d1675b7bb8d5';
// staging
const stagingHubBaseUrl = 'https://staginglogin.leomed.co';
const stagingHubClientId = 'ffff21d3-a7b9-4b3b-8939-d84b4fe76ada';


export const environment = {
  production: true,
  subdomain: subdomain,
  apiUrl: `https://${subdomain}.leomed.co/api/v1`,
  wsUrl: `wss://${subdomain}.leomed.co/api/ws`,
  hubBaseUrl: isStagingEnv ? stagingHubBaseUrl : hubBaseUrl,
  hubApiUrl: `${isStagingEnv ? stagingHubBaseUrl : hubBaseUrl}/api/v1`,
  hubClientId: isStagingEnv ? stagingHubClientId : hubClientId,
  googlePlacesApiKey: 'AIzaSyAN2IkGfeFgil49HHs0LehCPCne59p2X2Y',
}
