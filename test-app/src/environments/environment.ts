// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const hubBaseUrl = 'http://localhost:3001';

export const environment = {
  production: false,
  subdomain: 'dev',
  apiUrl: 'http://localhost:3000/api/v1',
  wsUrl: 'ws://localhost:5200/api/ws',
  hubBaseUrl: hubBaseUrl,
  hubApiUrl: `${hubBaseUrl}/api/v1`,
  hubClientId: '68d97b26-17f6-43f9-82f9-c004e123343d',
  googlePlacesApiKey: 'AIzaSyAN2IkGfeFgil49HHs0LehCPCne59p2X2Y'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
