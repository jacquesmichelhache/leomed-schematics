import { HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

// Cette fonction est un helper qui créer un object HttpParams avec un simple object pour passer des paramètres à une requête.
export const setHttpParams = (params: object): HttpParams => {
  let httpParams = new HttpParams();

  for (const paramName in params) {
    if (params.hasOwnProperty(paramName)) {
      httpParams = httpParams.append(paramName, params[paramName]);
    }
  }

  return httpParams;
};

export const isLeomedApi = (url: string): boolean => {
  return url.startsWith(environment.hubApiUrl) || url.startsWith(environment.apiUrl)
}

export const isExternalAccess = (): boolean => {
  return window.location.pathname.startsWith('/embedded/')
}

const REPLACE_BY_CURRENT_LANG = '%currentLang%';
export const mapPreferencesToParams = (preferences: any, currentLang: string = null): any => {
  // we remove unwanted params in the url
  const params: any = {}
  // We process the sort param
  const _sort = preferences.sort;
  const _sort_by = Object.keys(_sort).reduce((sortBy, key) => {
    const value = _sort[key];
    if (_sort[key]) {
      sortBy = sortBy ? `${sortBy},` : '';
      const direction = _sort[key] === 'descend' ? 'desc' : 'asc';
      const realKey = key.replace(REPLACE_BY_CURRENT_LANG, currentLang);
      sortBy = sortBy.concat(`${realKey}:${direction}`);
    }
    return sortBy;
  }, null);
  if (_sort_by) {
    params.sort_by = _sort_by;
  }
  // We process the filters params
  const _filters = preferences.filters;
  Object.keys(_filters).forEach(key => {
    const filter = _filters[key];

    if (filter != null){
      if (typeof filter !== 'object') {
        switch (filter) {
          case '!true':
            params[key] = true;
            break;
          case '!false':
            params[key] = false;
            break;
          default:
            params[key] = filter;
            break;
        }
      }else if (Array.isArray(filter) && filter.length > 0){ // if non-empy array
        params[key] = filter;
      }else if (typeof filter === 'object' && Object.keys(filter).length > 0){ // if non-empty hash
        params[key] = 'json:' + JSON.stringify(filter);
      }
    }
  });

  return params;
};
