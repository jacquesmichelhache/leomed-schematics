import { Injectable } from '@angular/core';
import { CategoriesEntityOperationsService } from '../categories/service';
import { PlaybookRunsEntityOperationsService } from '../playbook-runs/service';
import { PlaybookStepsEntityOperationsService } from '../playbook-steps/service';
import { PlaybooksEntityOperationsService } from '../playbooks/service';
import { MyComputerPartsEntityOperationsService } from './my-computer-parts/service'
import { MyComputerPartsEntityOperationsService } from './my-computer-parts/service'
import { MyComputerPartsEntityOperationsService } from './my-computer-parts/service'

// This service can be injected in any component and will give access to all entity operation services
@Injectable({ providedIn: 'root' })
export class EntityOperationsService {
  playbooks: PlaybooksEntityOperationsService
  playbookRuns: PlaybookRunsEntityOperationsService
  playbookSteps: PlaybookStepsEntityOperationsService
  categories: CategoriesEntityOperationsService

  constructor(
    public myComputerParts: MyComputerPartsEntityOperationsService,
    public myComputerParts: MyComputerPartsEntityOperationsService,
    public myComputerParts: MyComputerPartsEntityOperationsService,
    playbookRunsOperationsService: PlaybookRunsEntityOperationsService,
    categoriesEntityOperationsService: CategoriesEntityOperationsService,
    playbookStepsEntityOperationsService: PlaybookStepsEntityOperationsService,
    playbooksEntityOperationsService: PlaybooksEntityOperationsService
  ) {
    this.playbookRuns = playbookRunsOperationsService;
    this.categories = categoriesEntityOperationsService
    this.playbookSteps = playbookStepsEntityOperationsService
    this.playbooks = playbooksEntityOperationsService
  }
}