import { IndexResponse } from "./index-response";

export interface EntitiesState<T> extends IndexResponse<T> {
  selected: T;
  loading: boolean;
  saveStatus: 'failure' | 'success' | 'none';
}
