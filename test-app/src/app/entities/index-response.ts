export class IndexResponse<T> {
  page: number;
  page_size: number;
  total: number;
  data: T[];
  stats?: any; // statistics on data received (total count, minimum values, max, averages, ...)
}
