import {
  createSelector,
  createFeatureSelector,
  ActionReducerMap,
} from '@ngrx/store';
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'
import * as fromMyComputerParts from './my-computer-parts/reducers'

export const entitiesReducers: ActionReducerMap<any> = {
  applicationSettings: fromApplicationSettings.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
  myComputerParts: fromMyComputerParts.reducer,
};

export const getApplicationSettingState = createFeatureSelector<any>('applicationSettings')
export const getApplicationSetting = createSelector(getApplicationSettingState, fromApplicationSettings.getApplicationSetting)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)

export const getMyComputerPartsState = createFeatureSelector<any>('myComputerParts')
export const getMyComputerParts = createSelector(getMyComputerPartsState, fromMyComputerParts.getMyComputerParts)
export const getSelectedMyComputerPart = createSelector(getMyComputerPartsState, fromMyComputerParts.getSelectedMyComputerPart)
export const getMyComputerPartIsLoading = createSelector(getMyComputerPartsState, fromMyComputerParts.isLoading)
