import { Action } from '@ngrx/store'
import { EntityOptions } from '../shared/classes'
import { MyComputerPart } from './classes'

export const LOAD_MY_COMPUTER_PARTS = '[MyComputerParts] Load MyComputerParts'
export const UNSELECT_MY_COMPUTER_PART = '[MyComputerParts] Unselect MyComputerPart'
export const SELECT_MY_COMPUTER_PART = '[MyComputerParts] Select MyComputerPart'
export const GET_MY_COMPUTER_PART = '[MyComputerParts] Get MyComputerPart'
export const CREATE_MY_COMPUTER_PART = '[MyComputerParts] Create MyComputerPart'
export const UPDATE_MY_COMPUTER_PART = '[MyComputerParts] Update MyComputerPart'
export const DELETE_MY_COMPUTER_PART = '[MyComputerParts] Delete MyComputerPart'
export const CLEAR_MY_COMPUTER_PARTS = '[MyComputerParts] Clear MyComputerParts'
export const ACTION_DONE = '[MyComputerParts] Action Done'


export interface MyComputerPartsEntityOptions extends EntityOptions<MyComputerPart> {
  // add extra options here
}

export const defaultMyComputerPartsOptions:  MyComputerPartsEntityOptions = {
  // extra option default values
}

export class LoadMyComputerParts implements Action {
  readonly type = LOAD_MY_COMPUTER_PARTS
  constructor(public params: any = {}, public options: Partial<MyComputerPartsEntityOptions>  = defaultMyComputerPartsOptions) {}
}

export class UnselectMyComputerPart implements Action {
  readonly type = UNSELECT_MY_COMPUTER_PART
  constructor() {}
}

export class SelectMyComputerPart implements Action {
  readonly type = SELECT_MY_COMPUTER_PART
  constructor(public object: MyComputerPart) {}
}

export class GetMyComputerPart implements Action {
  readonly type = GET_MY_COMPUTER_PART
  constructor(public id: number, public options: Partial<MyComputerPartsEntityOptions>  = defaultMyComputerPartsOptions) {}
}

export class CreateMyComputerPart implements Action {
  readonly type = CREATE_MY_COMPUTER_PART
  constructor(public data: Partial<MyComputerPart>, public options: Partial<MyComputerPartsEntityOptions> = defaultMyComputerPartsOptions) {}
}

export class UpdateMyComputerPart implements Action {
  readonly type = UPDATE_MY_COMPUTER_PART
  constructor(public id: number, public data: MyComputerPart | any, public options: Partial<MyComputerPartsEntityOptions>  = defaultMyComputerPartsOptions) {}
}

export class DeleteMyComputerPart implements Action {
  readonly type = DELETE_MY_COMPUTER_PART
  constructor(public id: number, public options: Partial<MyComputerPartsEntityOptions>  = defaultMyComputerPartsOptions) {}
}

export class ActionDone implements Action {
  readonly type = ACTION_DONE
  constructor(public newState: any = {}, public options: Partial<MyComputerPartsEntityOptions>  = defaultMyComputerPartsOptions) {}
}

export class ClearMyComputerParts implements Action {
  readonly type = CLEAR_MY_COMPUTER_PARTS
  constructor() {}
}

export type Actions = 
LoadMyComputerParts
| UnselectMyComputerPart
| SelectMyComputerPart
| GetMyComputerPart
| CreateMyComputerPart
| DeleteMyComputerPart
| ClearMyComputerParts
| UpdateMyComputerPart
| ActionDone 
