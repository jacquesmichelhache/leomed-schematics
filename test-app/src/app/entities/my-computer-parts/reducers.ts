import * as actions from './actions'
import { EntitiesState } from '../entities-state'
import { MyComputerPart } from './classes'

export const initialState: EntitiesState<MyComputerPart> = {
  page: null,
  page_size: null,
  total: null,
  data: [],
  selected: null,
  loading: false,
  saveStatus: 'none',
}

export function reducer(state = initialState, action: actions.Actions): EntitiesState<MyComputerPart> {
  switch (action.type) {
    case actions.CLEAR_MY_COMPUTER_PARTS:
      return { ...initialState }
    case actions.UNSELECT_MY_COMPUTER_PART:
      return {
        ...state,
        selected: null,
        loading: false,
        saveStatus: 'none',
      }
    case actions.SELECT_MY_COMPUTER_PART:
      return {
        ...state,
        selected: action.object,
        loading: false,
      }
    case actions.GET_MY_COMPUTER_PART:
      return {
        ...state,
        selected: null,
        loading: true,
      }
    case actions.LOAD_MY_COMPUTER_PARTS:
    case actions.CREATE_MY_COMPUTER_PART:
    case actions.UPDATE_MY_COMPUTER_PART:
    case actions.DELETE_MY_COMPUTER_PART:
      return {
        ...state,
        loading: true,
        saveStatus: 'none',
      }
    case actions.ACTION_DONE:
      return {
        ...state,
        ...(action?.options?.skipStateChange ? {} : action.newState),
        loading: false,
      }
    default:
      return state
  }
}

export const getMyComputerParts = (state: EntitiesState<MyComputerPart>) => state.data
export const getSelectedMyComputerPart = (state: EntitiesState<MyComputerPart>) => state.selected
export const isLoading = (state:EntitiesState<MyComputerPart>) => state.loading
