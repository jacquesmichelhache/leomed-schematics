import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IndexResponse } from '../index-response';
import { environment } from 'src/environments/environment';
import { setHttpParams } from 'src/app/helpers/http-helpers';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { MyComputerPart } from './classes'
import * as MyComputerPartsActions from './actions'
import { MyComputerPartsEntityOptions } from './actions'
import { EntityServiceBase } from '../shared/entity-service-base';


// data service. This is where http requests are sent to API
@Injectable({ providedIn: 'root' })
export class MyComputerPartsDataService {
  constructor(private http: HttpClient) { }

  getMyComputerParts$(params?: object): Observable<IndexResponse<MyComputerPart>> {
    return this.http.get<IndexResponse<MyComputerPart>>(`${environment.apiUrl}/my_computer_parts`, { params: setHttpParams(params) })
  }

  getMyComputerPart$(id: number): Observable<MyComputerPart> {
    return this.http.get<MyComputerPart>(`${environment.apiUrl}/my_computer_parts/${id}`)
  }

  createMyComputerPart$(data: Partial<MyComputerPart> | FormData): Observable<MyComputerPart> {
    return this.http.post<MyComputerPart>(`${environment.apiUrl}/my_computer_parts`, { my_computer_part: data })
  }

  updateMyComputerPart$(id: number, data: Partial<MyComputerPart> | FormData): Observable<MyComputerPart> {
    return this.http.patch<MyComputerPart>(`${environment.apiUrl}/my_computer_parts/${id}`, { my_computer_part: data })
  }

  deleteMyComputerPart$(id: number): Observable<MyComputerPart> {
    return this.http.delete<MyComputerPart>(`${environment.apiUrl}/my_computer_parts/${id}`)
  }
}

// Entity operations service facad implementing correlationId
@Injectable({ providedIn: 'root' })
export class MyComputerPartsEntityOperationsService {
  constructor(private rxStore: Store, private entityServiceBase: EntityServiceBase) {}

  getAll(options: Partial<MyComputerPartsEntityOptions> = MyComputerPartsActions.defaultMyComputerPartsOptions): Observable<MyComputerPart[]> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.entityServiceBase.result<MyComputerPart[]>(corrId, MyComputerPartsActions.ACTION_DONE)
    this.rxStore.dispatch(new MyComputerPartsActions.LoadMyComputerParts(null, {  ...options, corrId: corrId }))
    return result$
  }

  getWithQuery(params: Object, options: Partial<MyComputerPartsEntityOptions> = MyComputerPartsActions.defaultMyComputerPartsOptions): Observable<MyComputerPart[]> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.entityServiceBase.result<MyComputerPart[]>(corrId, MyComputerPartsActions.ACTION_DONE)
    this.rxStore.dispatch(new MyComputerPartsActions.LoadMyComputerParts(params, {  ...options, corrId: corrId }))
    return result$
  }

  getById(id: number, options: Partial<MyComputerPartsEntityOptions> = MyComputerPartsActions.defaultMyComputerPartsOptions): Observable<MyComputerPart> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.entityServiceBase.result<MyComputerPart>(corrId, MyComputerPartsActions.ACTION_DONE)
    this.rxStore.dispatch(new MyComputerPartsActions.GetMyComputerPart(id, {  ...options, corrId: corrId }))
    return result$
  }

  add(MyComputerPart: Partial<MyComputerPart>, options: Partial<MyComputerPartsEntityOptions> = MyComputerPartsActions.defaultMyComputerPartsOptions): Observable<MyComputerPart> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.entityServiceBase.result<MyComputerPart>(corrId, MyComputerPartsActions.ACTION_DONE)
    this.rxStore.dispatch(new MyComputerPartsActions.CreateMyComputerPart(MyComputerPart, {  ...options, corrId: corrId }))
    return result$
  }

  update(id: number, MyComputerPart: Partial<MyComputerPart>, options: Partial<MyComputerPartsEntityOptions> = MyComputerPartsActions.defaultMyComputerPartsOptions): Observable<MyComputerPart> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.entityServiceBase.result<MyComputerPart>(corrId, MyComputerPartsActions.ACTION_DONE)
    this.rxStore.dispatch(new MyComputerPartsActions.UpdateMyComputerPart(id, MyComputerPart, {  ...options, corrId: corrId }))
    return result$
  }

  delete(id: number, options: Partial<MyComputerPartsEntityOptions> = MyComputerPartsActions.defaultMyComputerPartsOptions): Observable<MyComputerPart> {
    const corrId = this.entityServiceBase.generateUniqueId()
    const result$ = this.entityServiceBase.result<MyComputerPart>(corrId, MyComputerPartsActions.ACTION_DONE)
    this.rxStore.dispatch(new MyComputerPartsActions.DeleteMyComputerPart(id, {  ...options, corrId: corrId }))
    return result$
  }
}
