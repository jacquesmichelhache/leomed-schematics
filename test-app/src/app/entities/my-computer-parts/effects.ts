import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Actions, createEffect, ofType } from '@ngrx/effects'
import { Action, Store } from '@ngrx/store'
import { of, from, forkJoin, Observable } from 'rxjs'
import { map, switchMap, catchError, withLatestFrom, concatMap, mergeMap, exhaustMap } from 'rxjs/operators'
import { NzNotificationService } from 'ng-zorro-antd/notification'

import * as EntitiesReducers from '../reducers'
import * as MyComputerPartsActions from './actions'
import { TranslateService } from '@ngx-translate/core'
import { MyComputerPartsDataService } from './service'
import { ModalsService } from 'src/app/services/modals'
import { BaseEffectsClass } from 'src/app/store/base-effects-class'

@Injectable()
export class MyComputerPartsEffects extends BaseEffectsClass {
  BASE_ROUTE = '/:tenant/...' // schematic says user should complete base_route url path

  constructor(
    router: Router,
    private rxStore: Store,
    private actions$: Actions,
    private myComputerPartsDataService: MyComputerPartsDataService,
    private translate: TranslateService,
    private notification: NzNotificationService,
    private modal: ModalsService,
  ) {
    super(router)
  }

  loadMyComputerParts$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<MyComputerPartsActions.LoadMyComputerParts>(MyComputerPartsActions.LOAD_MY_COMPUTER_PARTS),
      concatMap(action => of(action).pipe(withLatestFrom(this.rxStore.select(EntitiesReducers.getMyComputerPartsState)))),
      mergeMap(([action, latestState]) => {
        return this.myComputerPartsDataService
          .getMyComputerParts$({
            page: latestState.page,
            page_size: latestState.page_size,
            ...action.params,
          })
          .pipe(
            map(response => {
              return new MyComputerPartsActions.ActionDone({ ...response }, { ...action.options, data: response.data })
            }),
            catchError(error => {
              if (!action.options.disableNotifications){
                this.notification.error(
                  this.translate.instant('errors.unknown_error'),
                  this.translate.instant('errors.unknown_error_description'),
                )
              }
              return from([new MyComputerPartsActions.ActionDone({}, {  ...action.options, data: undefined, errors: error.error })])
            }),
          )
      }),
    ),
  )

  getMyComputerPart$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<MyComputerPartsActions.GetMyComputerPart>(MyComputerPartsActions.GET_MY_COMPUTER_PART),
      concatMap(action => of(action).pipe(withLatestFrom(this.rxStore.select(EntitiesReducers.getMyComputerPartsState)))),
      mergeMap(([action, latestState]) => {
        let found
        if (latestState.selected && latestState.selected.id === action.id) {
          found = latestState.selected
        }
        if (found) {
          const actions: Action[] = []
          if (!action?.options?.skipEntitySelection) {
            actions.push(new MyComputerPartsActions.SelectMyComputerPart(found))
          }
          actions.push(new MyComputerPartsActions.ActionDone({},  { ...action.options, data: found }))
          return from(actions)
        } else {
          return this.myComputerPartsDataService.getMyComputerPart$(action.id).pipe(
            mergeMap(response => {

              const actions: Action[] = []
              if (!action?.options?.skipEntitySelection) {
                actions.push(new MyComputerPartsActions.SelectMyComputerPart(response))
              }
              actions.push(new MyComputerPartsActions.ActionDone({},  { ...action.options, data: response }))

              return from(actions)
            }),
            catchError(error => {
              if (!action.options.disableNotifications){
                if (error.status === 404) {
                  this.notification.error(
                    this.translate.instant('errors.page_not_found'),
                    this.translate.instant('errors.resource_not_found_description'),
                  )
                  this.router.navigate(['/auth/404'], { replaceUrl: true })
                } else {
                  this.notification.error(
                    this.translate.instant('errors.unknown_error'),
                    this.translate.instant('errors.unknown_error_description'),
                  )
                }
              }
              return from([new MyComputerPartsActions.ActionDone({}, { ...action.options, data: undefined, errors: error.error })])
            }),
          )
        }
      }),
    ),
  )

  createMyComputerPart$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<MyComputerPartsActions.CreateMyComputerPart>(MyComputerPartsActions.CREATE_MY_COMPUTER_PART),
      mergeMap(action => {
        return this.myComputerPartsDataService.createMyComputerPart$(action.data).pipe(
          mergeMap(response => {
            if (!action.options.disableNotifications){
              this.notification.success(this.translate.instant('shared.action_success'), '')
            }
           
           
            const actions: Action[] = []
            if (!action?.options?.skipEntitySelection) {
                actions.push(new MyComputerPartsActions.SelectMyComputerPart(response))
            }
            actions.push(new MyComputerPartsActions.ActionDone({ saveStatus: 'success' },  { ...action.options, data: response }))
            
            return from(actions)
          }),
          catchError(error => {
            if (!action.options.disableNotifications){
              if (error.status === 422) {
                this.notification.error(
                  this.translate.instant('errors.validations_failed'),
                  this.translate.instant('errors.validations_failed_description'),
                )
              } else {
                this.notification.error(
                  this.translate.instant('errors.unknown_error'),
                  this.translate.instant('errors.unknown_error_description'),
                )
              }
            }
            return from([new MyComputerPartsActions.ActionDone({ saveStatus: 'failure' }, { ...action.options, data: undefined, errors: error.error })])
          }),
        )
      }),
    ),
  )

  updateMyComputerPart$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<MyComputerPartsActions.UpdateMyComputerPart>(MyComputerPartsActions.UPDATE_MY_COMPUTER_PART),
      mergeMap(action => {
        return this.myComputerPartsDataService.updateMyComputerPart$(action.id, action.data).pipe(
          mergeMap(response => {
            if (!action.options.disableNotifications){
              this.notification.success(this.translate.instant('shared.action_success'), '')
            }
           

            const actions: Action[] = []
            if (!action?.options?.skipEntitySelection) {
                actions.push(new MyComputerPartsActions.SelectMyComputerPart(response))
            }
            actions.push(new MyComputerPartsActions.ActionDone({ saveStatus: 'success' },  { ...action.options, data: response }))

            return from(actions)
          }),
          catchError(error => {
            if (!action.options.disableNotifications){
              if (error.status === 422) {
                this.notification.error(
                  this.translate.instant('errors.validations_failed'),
                  this.translate.instant('errors.validations_failed_description'),
                )
              } else {
                this.notification.error(
                  this.translate.instant('errors.unknown_error'),
                  this.translate.instant('errors.unknown_error_description'),
                )
              }
            }            
            return from([new MyComputerPartsActions.ActionDone({ saveStatus: 'failure' }, { ...action.options, data: undefined, errors: error.error })])
          }),
        )
      }),
    ),
  )

  deleteMyComputerPart$: Observable<any> = createEffect(() =>
    this.actions$.pipe(
      ofType<MyComputerPartsActions.DeleteMyComputerPart>(MyComputerPartsActions.DELETE_MY_COMPUTER_PART),
      mergeMap(action =>
        forkJoin([
          of(action),
          this.modal.confirm({
            nzTitle: this.translate.instant('shared.confirm_delete', {
              entity: this.translate.instant('entities.my_computer_parts.this'),
            }),
            nzOkText: this.translate.instant('shared.yes'),
            nzOkType: 'danger',
            nzCancelText: this.translate.instant('shared.no'),
          }),
        ]),
      ),
      mergeMap(([action, confirm]) => {
        if (!confirm) {
          return of(new MyComputerPartsActions.ActionDone({}, { ...action.options, data: undefined }))
        }

        return this.myComputerPartsDataService.deleteMyComputerPart$(action.id).pipe(
          mergeMap(deletedMyComputerPart => {
            this.notification.success(this.translate.instant('shared.action_success'), '')
            return [
              new MyComputerPartsActions.ActionDone({ saveStatus: 'success' }, { ...action.options, data: deletedMyComputerPart }),
              new MyComputerPartsActions.UnselectMyComputerPart(),
            ]
          }),
          catchError(error => {
            if (!action.options.disableNotifications){
              if (error.status === 422) {
                this.notification.error(
                  this.translate.instant('errors.validations_failed'),
                  this.translate.instant('errors.entity_dependencies_delete_error_description'),
                )
              } else {
                this.notification.error(
                  this.translate.instant('errors.unknown_error'),
                  this.translate.instant('errors.unknown_error_description'),
                )
              }
            }            
            return from([new MyComputerPartsActions.ActionDone({ saveStatus: 'failure' }, { ...action.options, data: undefined, errors: error.error })])
          }),
        )
      }),
    ),
  )
}
