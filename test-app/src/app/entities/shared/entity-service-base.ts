import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects'
import { filter, take } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class EntityServiceBase {
  uniqueIdGenerator = 0;

  constructor(private actions$: Actions) { }

  generateUniqueId(): string {
    return `correlationId-${++this.uniqueIdGenerator}`;
  }

  result<T>(corrId: string, ACTION_DONE: string): Observable<T> {
    const replaySubject = new ReplaySubject<T>(1)
    this.actions$
      .pipe(
        ofType<any>(ACTION_DONE),
        filter(action => corrId === action?.options?.corrId),
        take(1),
      )
      .subscribe(action => {
        if (action.options.errors) {
          replaySubject.error(action.options.errors)
        } else {
          replaySubject.next(action.options.data as T)
          replaySubject.complete()
        }
      })
    return replaySubject
  }
}
