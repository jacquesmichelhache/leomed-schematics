export interface EntityOptions<T> {
  corrId?: string
  data?: T | T[]
  errors?: any

  // these options must be implemented in each entity for them to work
  skipNavigation?: boolean
  skipStateChange?: boolean
  skipEntitySelection?: boolean
  skipEntitiesReload?: boolean
  disableNotifications?: boolean
}