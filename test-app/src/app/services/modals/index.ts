import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NzModalService } from 'ng-zorro-antd/modal';
import { TranslateService } from '@ngx-translate/core';
import { FormControl, Validators } from '@angular/forms';
import { ModalContent2FAComponent } from 'src/app/components/leomed/layout/modal-2fa/modal-2fa.component';

// Simple wrapper around AntDesign modals to make then subscribable
@Injectable({ providedIn: 'root' })
export class ModalsService {
  constructor(private modal: NzModalService,
    private translate: TranslateService) { }

  confirm(options: any): Observable<boolean> {
    return new Observable<boolean>(subscriber => {
      this.modal.confirm({
        ...options,
        nzOnOk: () => {
          subscriber.next(true);
          subscriber.complete();
        },
        nzOnCancel: () => {
          subscriber.next(false);
          subscriber.complete();
        }
      });
    });
  }

  ask2FA(): Observable<string> {
    return new Observable<string>(subscriber => {
      const control2fa = new FormControl(null, [Validators.required]);
      this.modal.create({
        nzTitle: this.translate.instant('auth.2fa_title'),
        nzContent: ModalContent2FAComponent,
        nzOkText: this.translate.instant('shared.continue'),
        nzOnOk: (component: any) => {
          component.control2fa.markAsDirty();
          if (component.control2fa.invalid) {
            return false;
          } else {
            subscriber.next(component.control2fa.value);
            subscriber.complete();
          }
        },
        nzCancelText: this.translate.instant('shared.cancel'),
        nzOnCancel: () => {
          subscriber.next(null);
          subscriber.complete();
        }
      });
    });
  }
}
