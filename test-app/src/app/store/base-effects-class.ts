import { Router } from '@angular/router';

export class BaseEffectsClass {
  constructor(protected router: Router) {}

  requestNavigation(path: any[]) {
    // we don't redirect user if a navigation is already happening, it prevents important redirects from being cancelled
    if (!this.router.getCurrentNavigation()) {
      this.router.navigate(path);
    }
  }
}