import { AddEntityToReducerListContext } from "./add-entity-to-reducers-array";
import {Change, InsertChange} from "@schematics/angular/utility/change"
import { SchematicsException, Tree } from "@angular-devkit/schematics";
import { getSourceNodes } from "@schematics/angular/utility/ast-utils"
import * as ts from 'typescript';
import { strings } from '@angular-devkit/core';
// import * as fs from 'fs';

export function addEntitySelectorsChange(context: AddEntityToReducerListContext, tree: Tree): Change {
  let text = tree.read(context.path)
  if (!text) throw new SchematicsException('File does not exist.')
  let sourceText = text.toString('utf-8')

  // create the typescript source file
  let sourceFile = ts.createSourceFile(context.path, sourceText, ts.ScriptTarget.Latest, true)

  // get the nodes of the source file
  let nodes: ts.Node[] = getSourceNodes(sourceFile)

  const entitiesReducersNode = nodes?.[nodes.length - 1]

  if (!entitiesReducersNode) {
    throw new SchematicsException(`could not find a single node in ${context.path}`)
  }

  let reducerToAdd = `
export const get${strings.capitalize(context.options.entity_plural)}State = createFeatureSelector<any>('${context.options.entity_plural}')
export const get${strings.capitalize(context.options.entity_plural)} = createSelector(get${strings.capitalize(context.options.entity_plural)}State, from${strings.capitalize(context.options.entity_plural)}.get${strings.capitalize(context.options.entity_plural)})
export const getSelected${strings.capitalize(context.options.entity_singular)} = createSelector(get${strings.capitalize(context.options.entity_plural)}State, from${strings.capitalize(context.options.entity_plural)}.getSelected${strings.capitalize(context.options.entity_singular)})
export const get${strings.capitalize(context.options.entity_singular)}IsLoading = createSelector(get${strings.capitalize(context.options.entity_plural)}State, from${strings.capitalize(context.options.entity_plural)}.isLoading)
`

  return new InsertChange(context.path,  entitiesReducersNode.getEnd(), reducerToAdd)
}
